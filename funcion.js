const estudiante = async (req, res) => {
    const expresiones = {
		nombres: /^([a-zA-Z]+)*$/, 
        cedula: /^\d{10}$/,
        letras: /^[A-Za-z]{1,18}/
	}

    const {
        nombre, apellido, cedula, semestre, paralelo 
    } = req.body

    if(!expresiones.nombres.test(nombre) || !expresiones.nombres.test(apellido) || !expresiones.cedula.test(cedula)|| !expresiones.letras.test(semestre)|| !expresiones.letras.test(paralelo )){
        res.json({'error': 'true'})
    }else{
            try {
                    const newEst = await pool.query(
                    "INSERT INTO estudiante(nombre_est, apellido_est, ced_est, paralelo_est, semestre_est) VALUES($1, $2, $3, $4, $5) RETURNING *",
                    [nombre, apellido, cedula, paralelo, semestre]
                );
                if(newEst.rowCount == 1){
                    res.json(newEst.rows);
                }
            } catch (error) {
                res.json({'error': error})
            }
    }
}